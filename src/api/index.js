import axios from 'axios';

//新增 测试formdata数据
export const getFormTest = (data) => {
    return axios({
        method: 'POST',
        url: '/offlineorg/checkbody/test',
        data,
    })
}

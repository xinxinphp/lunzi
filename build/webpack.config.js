//npm run dev
//node build/2webpack.dev.conf
var path = require('path');
var webpack = require('webpack');


var HtmlWebpackPlugin = require('html-webpack-plugin');
var FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');

var extractTextPlugin = require('extract-text-webpack-plugin');

var CopyWebpackPlugin = require('copy-webpack-plugin');//拷贝资源插件


let entArr=()=>{
    if(process.argv[2]=='build'){//有参数就走这里

        //return [path.resolve(__dirname, '../src/index.js'),path.resolve(__dirname, '../src/ed.js')]

        return {
            entry: path.resolve(__dirname, '../src/index.js'),
            jquery: 'jquery',
        }

    }else {
        return ['./build/dev-client',path.resolve(__dirname, '../src/index.js')]
    }
}


module.exports = {
    entry: entArr(),
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: 'js/[name].js',
        publicPath: '/',
    },

    module: {
        rules: [

            {
                test: /\.css$/,
                use: extractTextPlugin.extract({
                    use: [
                        {loader: 'css-loader', options: {importLoaders: 1}},
                        'postcss-loader'
                    ],
                    fallback: 'style-loader',
                })
            },

            {
                test: /\.scss$/,
                use: extractTextPlugin.extract({
                    use: [
                        {loader: "css-loader"},
                        {loader: "sass-loader"},
                        {loader: 'postcss-loader'}
                    ],
                    fallback: "style-loader"
                })
            },
            {
                test: /\.(jsx|js)$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            "es2015", "react", "env"
                        ]
                    }
                },
                exclude: /node_modules/
            },

            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                loader: 'url-loader',//images进行抽离,只是针对css里面的img
                options: {
                    limit: 1024 * 5,//小于5KB进行Base64
                    name: 'images/[name].[ext]'
                }
            },

        ]
    },


    plugins: [
        // https://github.com/glenjamin/webpack-hot-middleware#installation--usage
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        // https://github.com/ampedandwired/html-webpack-plugin
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/index.html',
            inject: true,

        }),
        /* 多个html打包
        new HtmlWebpackPlugin({
            filename: 'index2.html',
            template: './src/index2.html',
            inject: true,
            chunks : ['entry2'],

        }),
        */
        new extractTextPlugin("css/index.css"),
        new FriendlyErrorsPlugin(),
        new webpack.ProvidePlugin({/*第一种引入jq*1.用npm 安装jquery,2.在这里直接引入,3.而后在项目全局即可使用*/
            $: "jquery",
            jquery: "jquery",
        }),

        //抽离入口文件为单独的js
        new webpack.optimize.CommonsChunkPlugin({
            name:['jquery'],//对应入口文件的jquery(单独抽离)
            filename:'js/[name].js',//抽离到哪里
            minChunks:1//抽离几个文件
        }),//优化


        //定义打包的时候直接拷贝的目录
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, '../static'),//定义要拷贝的源目录
                to: 'static',//定义要烤盘膛的目标目录
                //ignore: ['.*']//忽略拷贝指定的文件
            }
        ]),
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, '../src/images'),//定义要拷贝的源目录
                to: 'images',//定义要烤盘膛的目标目录
                //ignore: ['.*']//忽略拷贝指定的文件
            }
        ]),






    ],




}


const rm = require('rimraf')
const path = require('path')
var webpack = require('webpack')

var webpackConfig = require('./webpack.config.js')

const chalk = require('chalk')

let code=(process.argv[2] ? process.argv[2]:"没传参数"); //npm run build -- one //其中one 就是参数 -- 后面要有空格

console.log('> 传递的参数 < ' +code+ '\n')

rm(path.join(webpackConfig.output.path), err => {
    if (err) throw err
    webpack(webpackConfig, (err, stats) => {

        if (err) throw err

        console.log(chalk.cyan('  Build complete.\n'))
        console.log(chalk.yellow(
            '  Tip: built files are meant to be served over an HTTP server.\n' +
            '  Opening index.html over file:// won\'t work.\n'
        ))
    })
})

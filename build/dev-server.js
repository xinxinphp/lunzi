
var net = require('net');//检测端口是否被占用 必用的服务


// npm run dev

var opn = require('opn')
var express = require('express')
var webpack = require('webpack')

var webpackConfig = require('./webpack.config.js')

var port = 6868;

var app = express()

var compiler = webpack(webpackConfig)

var proxyMiddleware = require('http-proxy-middleware')
var proxyTable = require('./dev.ProxyTable.conf').proxyTable


Object.keys(proxyTable).forEach(function (context) {
    var options = proxyTable[context]
    if (typeof options === 'string') {
        options = { target: options }
    }
    app.use(proxyMiddleware(options.filter || context, options))
})


var devMiddleware = require('webpack-dev-middleware')(compiler, {
    publicPath: webpackConfig.output.publicPath,
    quiet: true
})

var hotMiddleware = require('webpack-hot-middleware')(compiler, {
    log: false,
    heartbeat: 2000
})

compiler.plugin('compilation', function (compilation) {
    compilation.plugin('html-webpack-plugin-after-emit', function (data, cb) {
        hotMiddleware.publish({ action: 'reload' })
        cb()
    })
})

app.use(devMiddleware)

app.use(hotMiddleware)

var uri = 'http://localhost:' + port


var portIsOccupied = function (port) {

    var server = net.createServer().listen(port)

    return new Promise(function (resolve, reject) {

        server.on('listening', function () { // 执行这块代码说明端口未被占用
            server.close() // 关闭服务
            console.log('\n The port【' + port + '】 is 端口未被占用.') // 控制台输出信息
            resolve()
        });
        server.on('error', function (err) {
            if (err.code === 'EADDRINUSE') { // 端口已经被使用
                code = false
                console.log('The port【' + port + '】 is occupied, please 端口已经被使用.')
                reject()
            }
        })
    })
}



portIsOccupied(port)
.then(function () {
    opn(uri)
    console.log('> Listening at ' + uri + '\n')
    app.listen(port)
})
.catch(function () {
    console.log('The port【' + port + '】 is occupied, please 端口已经被使用.')
})

